class babiesmpwarmodule {

#Configuración inicial:
  $symfony_root_path = '/www/ApiBabiesProject'
  $angular_root_path = '/www/FrontBabiesProject'
  $project_destination_path = '/www'

# Edit local /etc/hosts files to resolve some hostnames used on our application.
  host { 'localhost':
    ensure => 'present',
    target => '/etc/hosts',
    ip => '127.0.0.1',
    host_aliases => ['mysql']
  }

# Miscellaneous packages.
  $misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 'automake']
  package { $misc_packages: ensure => latest }

  class{'apache':}

  apache::vhost { "api-babier":
    port          => '80',
    docroot       => "$symfony_root_path/web",
    directory     => "$symfony_root_path/web",
    directory_options => 'Indexes FollowSymLinks MultiViews',
    directory_allow_override => 'All',
    template => 'babiesmpwarmodule/api_vhost.erb',
    server_name => 'api.babier.tk',
    serveraliases => 'api.babier.tk'
  }

  apache::vhost { "front-babier":
    port          => '80',
    docroot       => "$angular_root_path/app/public",
    directory     => "$angular_root_path/app/public",
    directory_options => 'Indexes FollowSymLinks MultiViews',
    directory_allow_override => 'All',
    template	  => 'babiesmpwarmodule/front_vhost.erb',
    server_name => 'babier.tk',
    serveraliases => 'www.babier.tk'
  }

  apache::vhost { "static-babier":
    port          => '80',
    docroot       => "$angular_root_path/app/images",
    directory     => "$angular_root_path/app/images",
    template	  => 'babiesmpwarmodule/static_vhost.erb',
    server_name => 'static.babier.tk',
    serveraliases => 'static.babier.tk'
  }

#MYSQL SERVER
  $root_mysql_password = 'babiermpwarroot'
  class { '::mysql::server':
    root_password    => "$root_mysql_password"
  }

# PHP WITH VERSION 5.6
  ::yum::managed_yumrepo { 'remi-php56':
    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
    enabled        => 1,
    gpgcheck       => 1,
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
    priority       => 1,
  }

  class { 'php':
    version => 'latest',
    require => Yumrepo['remi-php56']
  }

  php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap', 'pdo', 'mcrypt' ]: }

#Install Composer
  class { 'composer':
    command_name => 'composer',
    target_dir   => '/usr/local/bin'
  }

#Servicio para el control de la hora en el server
  class{'timezone':
    timezone => 'Europe/Madrid',
  }
  class { '::ntp':
    servers => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
    package_ensure => 'latest'
  }

#Instalación PDO
  exec {'install_pdo_packages':
    provider => shell,
    path => [ "/bin/", "/usr/bin/" ],
    command => "/usr/bin/yum install php-pdo -y;
                /usr/bin/yum install php-mysqlnd -y ;
                /usr/bin/yum install php-mysqli -y ;
                /usr/bin/yum install php-pdo.x86_64 -y;",
    notify  => Service["httpd"]
  }

#Configuración de GIT
  git::config { 'user.name':
    value => 'Xavier Serrat Bordas'
  }

  git::config { 'user.email':
    value => 'xavier.serrat22@gmail.com'
  }

#Deploy from repository
  vcsrepo { "$project_destination_path":
    ensure   => latest,
    provider => git,
    source => 'git@bitbucket.org:marcandres/pfm-babies-project.git',
    revision => 'master',
    force => true,
    notify => File["$symfony_root_path/app/config/parameters.yml"]
  }

  file { "$symfony_root_path/app/config/parameters.yml":
    mode   => 755,
    owner  => root,
    group  => root,
    source => "puppet:///modules/babiesmpwarmodule/parameters.yml",
    notify => Class['executebootstrapbabiermodule']
  }

#Instalación Sphinx Search
  class {'sphinxmodule':
    subscribe => Vcsrepo["$project_destination_path"],
    notify => Class['executebootstrapbabiermodule']
  }

#Bootstrap BabierProject (Instalación de librerías y otras dependencias del código fuente)
  class {'executebootstrapbabiermodule':
    require => Class['composer']
  }
}
